+++
title       = "Homepage"
description = "The description of the homepage"
sonstiges   = "Here's some more text"

showPages   = "" # Values 'all' or 'regular'

headline    = ""
+++

This text comes from `content/_index.md` and the following Font Awesome Icon as well.

[//]: # (stil = s (solid) | r (regular) | l (light). Regular and Light only in Pro-Version)
[//]: # (icon = Iconname without fa-)
[//]: # (color = Color)
[//]: # (size = Values from 1 - 10)

Here's the Font Awesome Icon through a shortcode: {{< fontawesome style="s" icon="arrow-from-left" size="2" color="green" >}}
