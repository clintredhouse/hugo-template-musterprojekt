#!/usr/bin/env bash

while true; do
  read -p "Did you customize htaccess, robots, config, Rake and deploy? (y/n)" yn
  case $yn in
    [Yy]* ) nps --silent clean.public;
            nps --silent clean.htaccess;
            nps --silent build.production.hugo;
            nps --silent copy.htaccess;
            nps --silent build.production.minifyHtml;
            nps --silent build.production.minifyXml;
            break;;

    [Nn]* ) clear;
            echo "---------------------------------------------------"
            echo "Okay, Please do that before going on..."
            echo "Customize .htaccess, humans, config, Rake and deploy"
            echo "and restart production-nomin build."
            echo "---------------------------------------------------"
            exit;;

      * ) echo "Please answer with Yes or No.";;
  esac
done
