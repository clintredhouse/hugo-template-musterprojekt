#!/usr/bin/env bash

# rsync can also be started with './Rakefile.rb'

# Variables
USER=my-user
HOST=my-server.com
DIR=my/directory/to/domain.tld/ # might sometimes be empty!
BUILD=build/public

echo
echo "-------------------------------------------------------------------------------"
echo "This script transfers the production files from $BUILD to the server."
echo "-------------------------------------------------------------------------------"
echo "Transfer in progress..."
echo

# Use Variables from above
# rsync Parameter mean:
#     a: transfer rights
#     v: verbose
#     z: Compress file transfer if slow connection
#     h: human-readable
#     e: Chooses remote shell (e. g. -e ssh)

rsync -avhe ssh --delete --exclude '.DS_Store' $BUILD $USER@$HOST:$DIR

echo
echo "...ready."
echo "-------------------------------------------------------------------------------"
echo
exit 0
